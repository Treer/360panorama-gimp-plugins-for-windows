Contains gimp source files and .lib files

For the source files, I happened to use version 2.9.22 from http://pirbot.com/mirrors/gimp/gimp/v2.8/gimp-2.8.22.tar.bz2 (via https://www.gimp.org/downloads/#mirrors)
and renamed config.h.win32 to config.h


For the .lib files, instructions for generating them are here
https://web.archive.org/web/20150906090521/http://registry.gimp.org/node/24883

however I've included the .lib files I generated in \lib

When filled out, your directory structure here should look something like this:
(only shows the directories that matter for building GIMP plugins)

�   config.h
�   readme.txt
�
+---build
�   +---windows
�           plug-ins.ico
�
+---lib
�       libgimp-2.0-0.lib
�       libgimpbase-2.0-0.lib
�       libgimpui-2.0-0.lib
�       libgimpwidgets-2.0-0.lib
�
+---libgimp
�       COPYING
�       gimp.c
�       gimp.def
�       gimp.h
�       gimpaspectpreview.c
�       gimpaspectpreview.h
�       gimpbrushes.c
�       gimpbrushes.h
�       gimpbrushes_pdb.c
�       gimpbrushes_pdb.h
�       gimpbrushmenu.c
�       gimpbrushmenu.h
�       gimpbrushselect.c
�       gimpbrushselect.h
�       gimpbrushselectbutton.c
�       gimpbrushselectbutton.h
�       gimpbrushselect_pdb.c
�       gimpbrushselect_pdb.h
�       gimpbrush_pdb.c
�       gimpbrush_pdb.h
�       gimpbuffer_pdb.c
�       gimpbuffer_pdb.h
�       gimpchannel.c
�       gimpchannel.h
�       gimpchannel_pdb.c
�       gimpchannel_pdb.h
�       gimpcolor_pdb.c
�       gimpcolor_pdb.h
�       gimpcompat.h
�       gimpcontext_pdb.c
�       gimpcontext_pdb.h
�       gimpconvert_pdb.c
�       gimpconvert_pdb.h
�       gimpdisplay_pdb.c
�       gimpdisplay_pdb.h
�       gimpdrawable.c
�       gimpdrawable.h
�       gimpdrawablepreview.c
�       gimpdrawablepreview.h
�       gimpdrawabletransform_pdb.c
�       gimpdrawabletransform_pdb.h
�       gimpdrawable_pdb.c
�       gimpdrawable_pdb.h
�       gimpdynamics_pdb.c
�       gimpdynamics_pdb.h
�       gimpedit_pdb.c
�       gimpedit_pdb.h
�       gimpenums.c
�       gimpenums.c.tail
�       gimpenums.h
�       gimpexport.c
�       gimpexport.h
�       gimpfileops_pdb.c
�       gimpfileops_pdb.h
�       gimpfloatingsel_pdb.c
�       gimpfloatingsel_pdb.h
�       gimpfontmenu.c
�       gimpfontmenu.h
�       gimpfontselect.c
�       gimpfontselect.h
�       gimpfontselectbutton.c
�       gimpfontselectbutton.h
�       gimpfontselect_pdb.c
�       gimpfontselect_pdb.h
�       gimpfonts_pdb.c
�       gimpfonts_pdb.h
�       gimpgimprc.c
�       gimpgimprc.h
�       gimpgimprc_pdb.c
�       gimpgimprc_pdb.h
�       gimpgradientmenu.c
�       gimpgradientmenu.h
�       gimpgradients.c
�       gimpgradients.h
�       gimpgradientselect.c
�       gimpgradientselect.h
�       gimpgradientselectbutton.c
�       gimpgradientselectbutton.h
�       gimpgradientselect_pdb.c
�       gimpgradientselect_pdb.h
�       gimpgradients_pdb.c
�       gimpgradients_pdb.h
�       gimpgradient_pdb.c
�       gimpgradient_pdb.h
�       gimpgrid_pdb.c
�       gimpgrid_pdb.h
�       gimpguides_pdb.c
�       gimpguides_pdb.h
�       gimphelp_pdb.c
�       gimphelp_pdb.h
�       gimpimage.c
�       gimpimage.h
�       gimpimagecombobox.c
�       gimpimagecombobox.h
�       gimpimageselect_pdb.c
�       gimpimageselect_pdb.h
�       gimpimage_pdb.c
�       gimpimage_pdb.h
�       gimpitemcombobox.c
�       gimpitemcombobox.h
�       gimpitemtransform_pdb.c
�       gimpitemtransform_pdb.h
�       gimpitem_pdb.c
�       gimpitem_pdb.h
�       gimplayer.c
�       gimplayer.h
�       gimplayer_pdb.c
�       gimplayer_pdb.h
�       gimpmenu.c
�       gimpmenu.h
�       gimpmessage_pdb.c
�       gimpmessage_pdb.h
�       gimppainttools_pdb.c
�       gimppainttools_pdb.h
�       gimppalette.c
�       gimppalette.h
�       gimppalettemenu.c
�       gimppalettemenu.h
�       gimppalettes.c
�       gimppalettes.h
�       gimppaletteselect.c
�       gimppaletteselect.h
�       gimppaletteselectbutton.c
�       gimppaletteselectbutton.h
�       gimppaletteselect_pdb.c
�       gimppaletteselect_pdb.h
�       gimppalettes_pdb.c
�       gimppalettes_pdb.h
�       gimppalette_pdb.c
�       gimppalette_pdb.h
�       gimppaths_pdb.c
�       gimppaths_pdb.h
�       gimppatternmenu.c
�       gimppatternmenu.h
�       gimppatterns.c
�       gimppatterns.h
�       gimppatternselect.c
�       gimppatternselect.h
�       gimppatternselectbutton.c
�       gimppatternselectbutton.h
�       gimppatternselect_pdb.c
�       gimppatternselect_pdb.h
�       gimppatterns_pdb.c
�       gimppatterns_pdb.h
�       gimppattern_pdb.c
�       gimppattern_pdb.h
�       gimppixbuf.c
�       gimppixbuf.h
�       gimppixelfetcher.c
�       gimppixelfetcher.h
�       gimppixelrgn.c
�       gimppixelrgn.h
�       gimpplugin.c
�       gimpplugin.h
�       gimpplugin_pdb.c
�       gimpplugin_pdb.h
�       gimpprocbrowserdialog.c
�       gimpprocbrowserdialog.h
�       gimpproceduraldb.c
�       gimpproceduraldb.h
�       gimpproceduraldb_pdb.c
�       gimpproceduraldb_pdb.h
�       gimpprocview.c
�       gimpprocview.h
�       gimpprogress.c
�       gimpprogress.h
�       gimpprogressbar.c
�       gimpprogressbar.h
�       gimpprogress_pdb.c
�       gimpprogress_pdb.h
�       gimpregioniterator.c
�       gimpregioniterator.h
�       gimpselectbutton.c
�       gimpselectbutton.h
�       gimpselection.c
�       gimpselection.h
�       gimpselectiontools_pdb.c
�       gimpselectiontools_pdb.h
�       gimpselection_pdb.c
�       gimpselection_pdb.h
�       gimptextlayer_pdb.c
�       gimptextlayer_pdb.h
�       gimptexttool_pdb.c
�       gimptexttool_pdb.h
�       gimptile.c
�       gimptile.h
�       gimptransformtools_pdb.c
�       gimptransformtools_pdb.h
�       gimptypes.h
�       gimpui.c
�       gimpui.def
�       gimpui.h
�       gimpuimarshal.c
�       gimpuimarshal.h
�       gimpuimarshal.list
�       gimpuitypes.h
�       gimpundo_pdb.c
�       gimpundo_pdb.h
�       gimpunitcache.c
�       gimpunitcache.h
�       gimpunit_pdb.c
�       gimpunit_pdb.h
�       gimpvectors.c
�       gimpvectors.h
�       gimpvectors_pdb.c
�       gimpvectors_pdb.h
�       gimpzoompreview.c
�       gimpzoompreview.h
�       gimp_pdb.c
�       gimp_pdb.h
�       gimp_pdb_headers.h
�       libgimp-intl.h
�       Makefile.am
�       Makefile.in
�       stdplugins-intl.h
�
+---libgimpbase
�       gimpbase-private.c
�       gimpbase-private.h
�       gimpbase.def
�       gimpbase.h
�       gimpbaseenums.c
�       gimpbaseenums.h
�       gimpbasetypes.c
�       gimpbasetypes.h
�       gimpchecks.c
�       gimpchecks.h
�       gimpcpuaccel.c
�       gimpcpuaccel.h
�       gimpdatafiles.c
�       gimpdatafiles.h
�       gimpenv.c
�       gimpenv.h
�       gimplimits.h
�       gimpmemsize.c
�       gimpmemsize.h
�       gimpparam.h
�       gimpparasite.c
�       gimpparasite.h
�       gimpparasiteio.c
�       gimpparasiteio.h
�       gimpprotocol.c
�       gimpprotocol.h
�       gimprectangle.c
�       gimprectangle.h
�       gimpreloc.c
�       gimpreloc.h
�       gimpsignal.c
�       gimpsignal.h
�       gimpunit.c
�       gimpunit.h
�       gimputils.c
�       gimputils.h
�       gimpversion.h
�       gimpwin32-io.h
�       gimpwire.c
�       gimpwire.h
�       Makefile.am
�       Makefile.in
�       test-cpu-accel.c
�
+---libgimpcolor
�       gimpadaptivesupersample.c
�       gimpadaptivesupersample.h
�       gimpbilinear.c
�       gimpbilinear.h
�       gimpcairocolor.c
�       gimpcairocolor.h
�       gimpcmyk.c
�       gimpcmyk.h
�       gimpcolor.def
�       gimpcolor.h
�       gimpcolormanaged.c
�       gimpcolormanaged.h
�       gimpcolorspace.c
�       gimpcolorspace.h
�       gimpcolortypes.h
�       gimphsl.c
�       gimphsl.h
�       gimphsv.c
�       gimphsv.h
�       gimprgb-parse.c
�       gimprgb.c
�       gimprgb.h
�       Makefile.am
�       Makefile.in
�       test-color-parser.c
�
+---libgimpconfig
�       gimpcolorconfig-enums.c
�       gimpcolorconfig-enums.h
�       gimpcolorconfig.c
�       gimpcolorconfig.h
�       gimpconfig-deserialize.c
�       gimpconfig-deserialize.h
�       gimpconfig-error.c
�       gimpconfig-error.h
�       gimpconfig-iface.c
�       gimpconfig-iface.h
�       gimpconfig-params.h
�       gimpconfig-path.c
�       gimpconfig-path.h
�       gimpconfig-serialize.c
�       gimpconfig-serialize.h
�       gimpconfig-utils.c
�       gimpconfig-utils.h
�       gimpconfig.def
�       gimpconfig.h
�       gimpconfigtypes.h
�       gimpconfigwriter.c
�       gimpconfigwriter.h
�       gimpscanner.c
�       gimpscanner.h
�       Makefile.am
�       Makefile.in
�
+---libgimpmath
        gimpmath.def
        gimpmath.h
        gimpmathtypes.h
        gimpmatrix.c
        gimpmatrix.h
        gimpmd5.c
        gimpmd5.h
        gimpvector.c
        gimpvector.h
        Makefile.am
        Makefile.in