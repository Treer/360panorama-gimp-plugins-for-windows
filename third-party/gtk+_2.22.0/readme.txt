Contains gtk source files and .lib files

I happened to use version 2.22.0 because that was the zip file linked from https://web.archive.org/web/20150906090521/http://registry.gimp.org/node/24883
and as noted in that guide, the .lib files are already built.

When filled out, your directory structure here should look something like this:
(only shows the directories that matter for building GIMP plugins)

�   readme.txt
+---include
�   �   autosprintf.h
�   �   expat.h
�   �   expat_external.h
�   �   ft2build.h
�   �   libintl.h
�   �   png.h
�   �   pngconf.h
�   �   zconf.h
�   �   zlib.h
�   �
�   +---atk-1.0
�   �   +---atk
�   �           atk-enum-types.h
�   �           atk.h
�   �           atkaction.h
�   �           atkcomponent.h
�   �           atkdocument.h
�   �           atkeditabletext.h
�   �           atkgobjectaccessible.h
�   �           atkhyperlink.h
�   �           atkhyperlinkimpl.h
�   �           atkhypertext.h
�   �           atkimage.h
�   �           atkmisc.h
�   �           atknoopobject.h
�   �           atknoopobjectfactory.h
�   �           atkobject.h
�   �           atkobjectfactory.h
�   �           atkplug.h
�   �           atkregistry.h
�   �           atkrelation.h
�   �           atkrelationset.h
�   �           atkrelationtype.h
�   �           atkselection.h
�   �           atksocket.h
�   �           atkstate.h
�   �           atkstateset.h
�   �           atkstreamablecontent.h
�   �           atktable.h
�   �           atktext.h
�   �           atkutil.h
�   �           atkvalue.h
�   �
�   +---cairo
�   �       cairo-deprecated.h
�   �       cairo-features.h
�   �       cairo-ft.h
�   �       cairo-gobject.h
�   �       cairo-pdf.h
�   �       cairo-ps.h
�   �       cairo-script-interpreter.h
�   �       cairo-svg.h
�   �       cairo-version.h
�   �       cairo-win32.h
�   �       cairo.h
�   �
�   +---fontconfig
�   �       fcfreetype.h
�   �       fcprivate.h
�   �       fontconfig.h
�   �
�   +---freetype2
�   �   +---freetype
�   �       �   freetype.h
�   �       �   ftadvanc.h
�   �       �   ftbbox.h
�   �       �   ftbdf.h
�   �       �   ftbitmap.h
�   �       �   ftcache.h
�   �       �   ftchapters.h
�   �       �   ftcid.h
�   �       �   fterrdef.h
�   �       �   fterrors.h
�   �       �   ftgasp.h
�   �       �   ftglyph.h
�   �       �   ftgxval.h
�   �       �   ftgzip.h
�   �       �   ftimage.h
�   �       �   ftincrem.h
�   �       �   ftlcdfil.h
�   �       �   ftlist.h
�   �       �   ftlzw.h
�   �       �   ftmac.h
�   �       �   ftmm.h
�   �       �   ftmodapi.h
�   �       �   ftmoderr.h
�   �       �   ftotval.h
�   �       �   ftoutln.h
�   �       �   ftpfr.h
�   �       �   ftrender.h
�   �       �   ftsizes.h
�   �       �   ftsnames.h
�   �       �   ftstroke.h
�   �       �   ftsynth.h
�   �       �   ftsystem.h
�   �       �   fttrigon.h
�   �       �   fttypes.h
�   �       �   ftwinfnt.h
�   �       �   ftxf86.h
�   �       �   t1tables.h
�   �       �   ttnameid.h
�   �       �   tttables.h
�   �       �   tttags.h
�   �       �   ttunpat.h
�   �       �
�   �       +---config
�   �               ftconfig.h
�   �               ftheader.h
�   �               ftmodule.h
�   �               ftoption.h
�   �               ftstdlib.h
�   �
�   +---gail-1.0
�   �   +---gail
�   �   �       gailwidget.h
�   �   �
�   �   +---libgail-util
�   �           gail-util.h
�   �           gailmisc.h
�   �           gailtextutil.h
�   �
�   +---gdk-pixbuf-2.0
�   �   +---gdk-pixbuf
�   �           gdk-pixbuf-animation.h
�   �           gdk-pixbuf-core.h
�   �           gdk-pixbuf-enum-types.h
�   �           gdk-pixbuf-features.h
�   �           gdk-pixbuf-io.h
�   �           gdk-pixbuf-loader.h
�   �           gdk-pixbuf-marshal.h
�   �           gdk-pixbuf-simple-anim.h
�   �           gdk-pixbuf-transform.h
�   �           gdk-pixbuf.h
�   �           gdk-pixdata.h
�   �
�   +---glib-2.0
�   �   �   glib-object.h
�   �   �   glib.h
�   �   �   gmodule.h
�   �   �
�   �   +---gio
�   �   �       gappinfo.h
�   �   �       gasyncinitable.h
�   �   �       gasyncresult.h
�   �   �       gbufferedinputstream.h
�   �   �       gbufferedoutputstream.h
�   �   �       gcancellable.h
�   �   �       gcharsetconverter.h
�   �   �       gcontenttype.h
�   �   �       gconverter.h
�   �   �       gconverterinputstream.h
�   �   �       gconverteroutputstream.h
�   �   �       gcredentials.h
�   �   �       gdatainputstream.h
�   �   �       gdataoutputstream.h
�   �   �       gdbusaddress.h
�   �   �       gdbusauthobserver.h
�   �   �       gdbusconnection.h
�   �   �       gdbuserror.h
�   �   �       gdbusintrospection.h
�   �   �       gdbusmessage.h
�   �   �       gdbusmethodinvocation.h
�   �   �       gdbusnameowning.h
�   �   �       gdbusnamewatching.h
�   �   �       gdbusproxy.h
�   �   �       gdbusserver.h
�   �   �       gdbusutils.h
�   �   �       gdrive.h
�   �   �       gemblem.h
�   �   �       gemblemedicon.h
�   �   �       gfile.h
�   �   �       gfileattribute.h
�   �   �       gfileenumerator.h
�   �   �       gfileicon.h
�   �   �       gfileinfo.h
�   �   �       gfileinputstream.h
�   �   �       gfileiostream.h
�   �   �       gfilemonitor.h
�   �   �       gfilenamecompleter.h
�   �   �       gfileoutputstream.h
�   �   �       gfilterinputstream.h
�   �   �       gfilteroutputstream.h
�   �   �       gicon.h
�   �   �       ginetaddress.h
�   �   �       ginetsocketaddress.h
�   �   �       ginitable.h
�   �   �       ginputstream.h
�   �   �       gio.h
�   �   �       gioenums.h
�   �   �       gioenumtypes.h
�   �   �       gioerror.h
�   �   �       giomodule.h
�   �   �       gioscheduler.h
�   �   �       giostream.h
�   �   �       giotypes.h
�   �   �       gloadableicon.h
�   �   �       gmemoryinputstream.h
�   �   �       gmemoryoutputstream.h
�   �   �       gmount.h
�   �   �       gmountoperation.h
�   �   �       gnativevolumemonitor.h
�   �   �       gnetworkaddress.h
�   �   �       gnetworkservice.h
�   �   �       goutputstream.h
�   �   �       gpermission.h
�   �   �       gproxy.h
�   �   �       gproxyaddress.h
�   �   �       gproxyaddressenumerator.h
�   �   �       gproxyresolver.h
�   �   �       gresolver.h
�   �   �       gseekable.h
�   �   �       gsettings.h
�   �   �       gsettingsbackend.h
�   �   �       gsimpleasyncresult.h
�   �   �       gsimplepermission.h
�   �   �       gsocket.h
�   �   �       gsocketaddress.h
�   �   �       gsocketaddressenumerator.h
�   �   �       gsocketclient.h
�   �   �       gsocketconnectable.h
�   �   �       gsocketconnection.h
�   �   �       gsocketcontrolmessage.h
�   �   �       gsocketlistener.h
�   �   �       gsocketservice.h
�   �   �       gsrvtarget.h
�   �   �       gtcpconnection.h
�   �   �       gthemedicon.h
�   �   �       gthreadedsocketservice.h
�   �   �       gvfs.h
�   �   �       gvolume.h
�   �   �       gvolumemonitor.h
�   �   �       gzlibcompressor.h
�   �   �       gzlibdecompressor.h
�   �   �
�   �   +---glib
�   �   �       galloca.h
�   �   �       garray.h
�   �   �       gasyncqueue.h
�   �   �       gatomic.h
�   �   �       gbacktrace.h
�   �   �       gbase64.h
�   �   �       gbitlock.h
�   �   �       gbookmarkfile.h
�   �   �       gcache.h
�   �   �       gchecksum.h
�   �   �       gcompletion.h
�   �   �       gconvert.h
�   �   �       gdataset.h
�   �   �       gdate.h
�   �   �       gdatetime.h
�   �   �       gdir.h
�   �   �       gerror.h
�   �   �       gfileutils.h
�   �   �       ghash.h
�   �   �       ghook.h
�   �   �       ghostutils.h
�   �   �       gi18n-lib.h
�   �   �       gi18n.h
�   �   �       giochannel.h
�   �   �       gkeyfile.h
�   �   �       glist.h
�   �   �       gmacros.h
�   �   �       gmain.h
�   �   �       gmappedfile.h
�   �   �       gmarkup.h
�   �   �       gmem.h
�   �   �       gmessages.h
�   �   �       gnode.h
�   �   �       goption.h
�   �   �       gpattern.h
�   �   �       gpoll.h
�   �   �       gprimes.h
�   �   �       gprintf.h
�   �   �       gqsort.h
�   �   �       gquark.h
�   �   �       gqueue.h
�   �   �       grand.h
�   �   �       gregex.h
�   �   �       grel.h
�   �   �       gscanner.h
�   �   �       gsequence.h
�   �   �       gshell.h
�   �   �       gslice.h
�   �   �       gslist.h
�   �   �       gspawn.h
�   �   �       gstdio.h
�   �   �       gstrfuncs.h
�   �   �       gstring.h
�   �   �       gtestutils.h
�   �   �       gthread.h
�   �   �       gthreadpool.h
�   �   �       gtimer.h
�   �   �       gtimezone.h
�   �   �       gtree.h
�   �   �       gtypes.h
�   �   �       gunicode.h
�   �   �       gurifuncs.h
�   �   �       gutils.h
�   �   �       gvariant.h
�   �   �       gvarianttype.h
�   �   �       gwin32.h
�   �   �
�   �   +---gobject
�   �           gbinding.h
�   �           gboxed.h
�   �           gclosure.h
�   �           genums.h
�   �           gmarshal.h
�   �           gobject.h
�   �           gobjectnotifyqueue.c
�   �           gparam.h
�   �           gparamspecs.h
�   �           gsignal.h
�   �           gsourceclosure.h
�   �           gtype.h
�   �           gtypemodule.h
�   �           gtypeplugin.h
�   �           gvalue.h
�   �           gvaluearray.h
�   �           gvaluecollector.h
�   �           gvaluetypes.h
�   �
�   +---gtk-2.0
�   �   +---gdk
�   �   �       gdk.h
�   �   �       gdkapplaunchcontext.h
�   �   �       gdkcairo.h
�   �   �       gdkcolor.h
�   �   �       gdkcursor.h
�   �   �       gdkdisplay.h
�   �   �       gdkdisplaymanager.h
�   �   �       gdkdnd.h
�   �   �       gdkdrawable.h
�   �   �       gdkenumtypes.h
�   �   �       gdkevents.h
�   �   �       gdkfont.h
�   �   �       gdkgc.h
�   �   �       gdki18n.h
�   �   �       gdkimage.h
�   �   �       gdkinput.h
�   �   �       gdkkeys.h
�   �   �       gdkkeysyms-compat.h
�   �   �       gdkkeysyms.h
�   �   �       gdkpango.h
�   �   �       gdkpixbuf.h
�   �   �       gdkpixmap.h
�   �   �       gdkprivate.h
�   �   �       gdkproperty.h
�   �   �       gdkregion.h
�   �   �       gdkrgb.h
�   �   �       gdkscreen.h
�   �   �       gdkselection.h
�   �   �       gdkspawn.h
�   �   �       gdktestutils.h
�   �   �       gdktypes.h
�   �   �       gdkvisual.h
�   �   �       gdkwin32.h
�   �   �       gdkwindow.h
�   �   �
�   �   +---gtk
�   �           gtk.h
�   �           gtkaboutdialog.h
�   �           gtkaccelgroup.h
�   �           gtkaccellabel.h
�   �           gtkaccelmap.h
�   �           gtkaccessible.h
�   �           gtkaction.h
�   �           gtkactiongroup.h
�   �           gtkactivatable.h
�   �           gtkadjustment.h
�   �           gtkalignment.h
�   �           gtkarrow.h
�   �           gtkaspectframe.h
�   �           gtkassistant.h
�   �           gtkbbox.h
�   �           gtkbin.h
�   �           gtkbindings.h
�   �           gtkbox.h
�   �           gtkbuildable.h
�   �           gtkbuilder.h
�   �           gtkbutton.h
�   �           gtkcalendar.h
�   �           gtkcelleditable.h
�   �           gtkcelllayout.h
�   �           gtkcellrenderer.h
�   �           gtkcellrendereraccel.h
�   �           gtkcellrenderercombo.h
�   �           gtkcellrendererpixbuf.h
�   �           gtkcellrendererprogress.h
�   �           gtkcellrendererspin.h
�   �           gtkcellrendererspinner.h
�   �           gtkcellrenderertext.h
�   �           gtkcellrenderertoggle.h
�   �           gtkcellview.h
�   �           gtkcheckbutton.h
�   �           gtkcheckmenuitem.h
�   �           gtkclipboard.h
�   �           gtkclist.h
�   �           gtkcolorbutton.h
�   �           gtkcolorsel.h
�   �           gtkcolorseldialog.h
�   �           gtkcombo.h
�   �           gtkcombobox.h
�   �           gtkcomboboxentry.h
�   �           gtkcontainer.h
�   �           gtkctree.h
�   �           gtkcurve.h
�   �           gtkdebug.h
�   �           gtkdialog.h
�   �           gtkdnd.h
�   �           gtkdrawingarea.h
�   �           gtkeditable.h
�   �           gtkentry.h
�   �           gtkentrybuffer.h
�   �           gtkentrycompletion.h
�   �           gtkenums.h
�   �           gtkeventbox.h
�   �           gtkexpander.h
�   �           gtkfilechooser.h
�   �           gtkfilechooserbutton.h
�   �           gtkfilechooserdialog.h
�   �           gtkfilechooserwidget.h
�   �           gtkfilefilter.h
�   �           gtkfilesel.h
�   �           gtkfixed.h
�   �           gtkfontbutton.h
�   �           gtkfontsel.h
�   �           gtkframe.h
�   �           gtkgamma.h
�   �           gtkgc.h
�   �           gtkhandlebox.h
�   �           gtkhbbox.h
�   �           gtkhbox.h
�   �           gtkhpaned.h
�   �           gtkhruler.h
�   �           gtkhscale.h
�   �           gtkhscrollbar.h
�   �           gtkhseparator.h
�   �           gtkhsv.h
�   �           gtkiconfactory.h
�   �           gtkicontheme.h
�   �           gtkiconview.h
�   �           gtkimage.h
�   �           gtkimagemenuitem.h
�   �           gtkimcontext.h
�   �           gtkimcontextsimple.h
�   �           gtkimmodule.h
�   �           gtkimmulticontext.h
�   �           gtkinfobar.h
�   �           gtkinputdialog.h
�   �           gtkinvisible.h
�   �           gtkitem.h
�   �           gtkitemfactory.h
�   �           gtklabel.h
�   �           gtklayout.h
�   �           gtklinkbutton.h
�   �           gtklist.h
�   �           gtklistitem.h
�   �           gtkliststore.h
�   �           gtkmain.h
�   �           gtkmarshal.h
�   �           gtkmenu.h
�   �           gtkmenubar.h
�   �           gtkmenuitem.h
�   �           gtkmenushell.h
�   �           gtkmenutoolbutton.h
�   �           gtkmessagedialog.h
�   �           gtkmisc.h
�   �           gtkmodules.h
�   �           gtkmountoperation.h
�   �           gtknotebook.h
�   �           gtkobject.h
�   �           gtkoffscreenwindow.h
�   �           gtkoldeditable.h
�   �           gtkoptionmenu.h
�   �           gtkorientable.h
�   �           gtkpagesetup.h
�   �           gtkpaned.h
�   �           gtkpapersize.h
�   �           gtkpixmap.h
�   �           gtkplug.h
�   �           gtkpreview.h
�   �           gtkprintcontext.h
�   �           gtkprintoperation.h
�   �           gtkprintoperationpreview.h
�   �           gtkprintsettings.h
�   �           gtkprivate.h
�   �           gtkprogress.h
�   �           gtkprogressbar.h
�   �           gtkradioaction.h
�   �           gtkradiobutton.h
�   �           gtkradiomenuitem.h
�   �           gtkradiotoolbutton.h
�   �           gtkrange.h
�   �           gtkrc.h
�   �           gtkrecentaction.h
�   �           gtkrecentchooser.h
�   �           gtkrecentchooserdialog.h
�   �           gtkrecentchoosermenu.h
�   �           gtkrecentchooserwidget.h
�   �           gtkrecentfilter.h
�   �           gtkrecentmanager.h
�   �           gtkruler.h
�   �           gtkscale.h
�   �           gtkscalebutton.h
�   �           gtkscrollbar.h
�   �           gtkscrolledwindow.h
�   �           gtkselection.h
�   �           gtkseparator.h
�   �           gtkseparatormenuitem.h
�   �           gtkseparatortoolitem.h
�   �           gtksettings.h
�   �           gtkshow.h
�   �           gtksignal.h
�   �           gtksizegroup.h
�   �           gtksocket.h
�   �           gtkspinbutton.h
�   �           gtkspinner.h
�   �           gtkstatusbar.h
�   �           gtkstatusicon.h
�   �           gtkstock.h
�   �           gtkstyle.h
�   �           gtktable.h
�   �           gtktearoffmenuitem.h
�   �           gtktestutils.h
�   �           gtktext.h
�   �           gtktextbuffer.h
�   �           gtktextbufferrichtext.h
�   �           gtktextchild.h
�   �           gtktextdisplay.h
�   �           gtktextiter.h
�   �           gtktextlayout.h
�   �           gtktextmark.h
�   �           gtktexttag.h
�   �           gtktexttagtable.h
�   �           gtktextview.h
�   �           gtktipsquery.h
�   �           gtktoggleaction.h
�   �           gtktogglebutton.h
�   �           gtktoggletoolbutton.h
�   �           gtktoolbar.h
�   �           gtktoolbutton.h
�   �           gtktoolitem.h
�   �           gtktoolitemgroup.h
�   �           gtktoolpalette.h
�   �           gtktoolshell.h
�   �           gtktooltip.h
�   �           gtktooltips.h
�   �           gtktree.h
�   �           gtktreednd.h
�   �           gtktreeitem.h
�   �           gtktreemodel.h
�   �           gtktreemodelfilter.h
�   �           gtktreemodelsort.h
�   �           gtktreeselection.h
�   �           gtktreesortable.h
�   �           gtktreestore.h
�   �           gtktreeview.h
�   �           gtktreeviewcolumn.h
�   �           gtktypebuiltins.h
�   �           gtktypeutils.h
�   �           gtkuimanager.h
�   �           gtkvbbox.h
�   �           gtkvbox.h
�   �           gtkversion.h
�   �           gtkviewport.h
�   �           gtkvolumebutton.h
�   �           gtkvpaned.h
�   �           gtkvruler.h
�   �           gtkvscale.h
�   �           gtkvscrollbar.h
�   �           gtkvseparator.h
�   �           gtkwidget.h
�   �           gtkwindow.h
�   �
�   +---libpng14
�   �       png.h
�   �       pngconf.h
�   �
�   +---pango-1.0
�   �   +---pango
�   �           pango-attributes.h
�   �           pango-bidi-type.h
�   �           pango-break.h
�   �           pango-context.h
�   �           pango-coverage.h
�   �           pango-engine.h
�   �           pango-enum-types.h
�   �           pango-features.h
�   �           pango-font.h
�   �           pango-fontmap.h
�   �           pango-fontset.h
�   �           pango-glyph-item.h
�   �           pango-glyph.h
�   �           pango-gravity.h
�   �           pango-item.h
�   �           pango-language.h
�   �           pango-layout.h
�   �           pango-matrix.h
�   �           pango-modules.h
�   �           pango-ot.h
�   �           pango-renderer.h
�   �           pango-script.h
�   �           pango-tabs.h
�   �           pango-types.h
�   �           pango-utils.h
�   �           pango.h
�   �           pangocairo.h
�   �           pangofc-decoder.h
�   �           pangofc-font.h
�   �           pangofc-fontmap.h
�   �           pangoft2.h
�   �           pangowin32.h
�   �
�   +---pixman-1
�           pixman-version.h
�           pixman.h
�
+---lib
    �   atk-1.0.def
    �   atk-1.0.lib
    �   cairo.def
    �   cairo.lib
    �   expat.lib
    �   fontconfig.def
    �   fontconfig.lib
    �   freetype.def
    �   freetype.lib
    �   gailutil.lib
    �   gdk-win32-2.0.lib
    �   gdk_pixbuf-2.0.lib
    �   gio-2.0.def
    �   gio-2.0.lib
    �   glib-2.0.def
    �   glib-2.0.lib
    �   gmodule-2.0.def
    �   gmodule-2.0.lib
    �   GNU.Gettext.dll
    �   gobject-2.0.def
    �   gobject-2.0.lib
    �   gthread-2.0.def
    �   gthread-2.0.lib
    �   gtk-win32-2.0.lib
    �   intl.lib
    �   libasprintf.dll.a
    �   libatk-1.0.dll.a
    �   libcairo.dll.a
    �   libexpat.def
    �   libexpat.dll.a
    �   libfontconfig.dll.a
    �   libfreetype.dll.a
    �   libgailutil.dll.a
    �   libgdk-win32-2.0.dll.a
    �   libgdk_pixbuf-2.0.dll.a
    �   libgio-2.0.dll.a
    �   libglib-2.0.dll.a
    �   libgmodule-2.0.dll.a
    �   libgobject-2.0.dll.a
    �   libgthread-2.0.dll.a
    �   libgtk-win32-2.0.dll.a
    �   libintl.def
    �   libintl.dll.a
    �   libpango-1.0.dll.a
    �   libpangocairo-1.0.dll.a
    �   libpangoft2-1.0.dll.a
    �   libpangowin32-1.0.dll.a
    �   libpixman-1.a
    �   libpng.def
    �   libpng.lib
    �   libpng14.dll.a
    �   libz.dll.a
    �   pango-1.0.def
    �   pango-1.0.lib
    �   pangocairo-1.0.def
    �   pangocairo-1.0.lib
    �   pangoft2-1.0.def
    �   pangoft2-1.0.lib
    �   pangowin32-1.0.def
    �   pangowin32-1.0.lib
    �   zdll.lib
    �   zlib.def
    �
    +---gdk-pixbuf-2.0
    �   +---2.10.0
    �       +---loaders
    �               loaders.cache
    �
    +---glib-2.0
    �   +---include
    �           glibconfig.h
    �
    +---gtk-2.0
    �   +---2.10.0
    �   �   +---engines
    �   �           libpixmap.dll
    �   �           libwimp.dll
    �   �
    �   +---include
    �   �       gdkconfig.h
    �   �
    �   +---modules
    �           libgail.dll
    �
    +---pkgconfig
            atk.pc
            cairo-fc.pc
            cairo-ft.pc
            cairo-gobject.pc
            cairo-pdf.pc
            cairo-png.pc
            cairo-ps.pc
            cairo-svg.pc
            cairo-win32-font.pc
            cairo-win32.pc
            cairo.pc
            fontconfig.pc
            freetype2.pc
            gail.pc
            gdk-2.0.pc
            gdk-pixbuf-2.0.pc
            gdk-win32-2.0.pc
            gio-2.0.pc
            gio-windows-2.0.pc
            glib-2.0.pc
            gmodule-2.0.pc
            gmodule-no-export-2.0.pc
            gobject-2.0.pc
            gthread-2.0.pc
            gtk+-2.0.pc
            gtk+-win32-2.0.pc
            libpng.pc
            libpng14.pc
            pango.pc
            pangocairo.pc
            pangoft2.pc
            pangowin32.pc
            pixman-1.pc