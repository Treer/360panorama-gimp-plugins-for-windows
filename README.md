﻿# 360 Panorama plugins for GIMP (Windows)

This plugin set contain four plugins helpful for editing 360° equirectangular panoramas, such as those created by Ricoh Theta:

* Panorama to zenith & nadir - converts panorama to editable form of zenith and nadir areas (looking straight up and looking straight down). This allows for easy editing, eg. removing a tripod, filling, placing a logo
* zenith & nadir to panorama - converts the editable zenith and nadir back into a panorama projection
* set Google+ metadata - attach metadata needed to publish image as a panorama on Google+
* Fill zenith & nadir - fills transparent areas on the top and the bottom of the generated panorama with a gradient that smoothly connects to the neighbour pixels

Examples of usage are shown in **[the tutorial](https://sourceforge.net/p/gimp360panorama/wiki/Tutorial/)**

## Download and installation

Download the plugins: [360panorama-gimp-plugins-for-windows.zip](https://gitlab.com/Treer/360panorama-gimp-plugins-for-windows/raw/binaries/Release/360panorama-gimp-plugins-for-windows.zip)

Unzip the file. To make the plugins available to all users, copy the 4 .exe files to `%ProgramFiles%\GIMP 2\lib\gimp\2.0\plug-ins\`

or, to make the plugins available to the current user, copy the 4 .exe files to `%USERPROFILE%\.gimp-2.8\plug-ins\`

The plugin directories may be in a different location on your computer, so if the 
locations given above are not correct you can find them by running GIMP and going to ***Edit → Preferences → Folders → Plugins***

## Credit

This project is a Windows recompiling of the Linux plugins that were written by Michał Rudewicz. The original 
project homepage of these plugins is [here](https://sourceforge.net/p/gimp360panorama/wiki/Home/), and 
the GIMP Registry page is [here](http://registry.gimp.org/node/27857)