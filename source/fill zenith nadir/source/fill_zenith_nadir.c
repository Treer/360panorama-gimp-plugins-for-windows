/**
 * Copyright 2013 Michał Rudewicz
 * 
 * Gimp plugin that fills the top and the bottom empty areas
 * 
 */

#include "config.h"
#include <libgimp/gimp.h>


static void query(void);
static void run(const gchar *name,
		gint nparams,
		const GimpParam *param,
		gint *nreturn_vals,
		GimpParam **return_vals);
static void fillZenithAndNadir(GimpDrawable *drawable);

GimpPlugInInfo PLUG_IN_INFO = {
	NULL,
	NULL,
	query,
	run
};

MAIN()

static void
query(void) {
	static GimpParamDef args[] = {
		{
			GIMP_PDB_INT32,
			"run-mode",
			"Run mode"
		},
		{
			GIMP_PDB_IMAGE,
			"image",
			"Input image"
		},
		{
			GIMP_PDB_DRAWABLE,
			"drawable",
			"Input drawable"
		}
	};

	gimp_install_procedure(
			"plug-in-fill-zenith-and-nadir",
			"Fill zenith and nadir",
			"Fills transparent zenith and nadir in 360 panorama with gradient",
			"Michał Rudewicz",
			"Copyright Michał Rudewicz",
			"2013",
			"_Fill zenith & nadir",
			"RGBA, GRAYA",
			GIMP_PLUGIN,
			G_N_ELEMENTS(args), 0,
			args, NULL);

	gimp_plugin_menu_register("plug-in-fill-zenith-and-nadir",
			"<Image>/Filters/360 Panorama");
}

static void
run(const gchar *name,
		gint nparams,
		const GimpParam *param,
		gint *nreturn_vals,
		GimpParam **return_vals) {
	static GimpParam values[1];
	GimpPDBStatusType status = GIMP_PDB_SUCCESS;
	GimpDrawable *drawable;
	gint32 imageId;

	/* Setting mandatory output values */
	*nreturn_vals = 1;
	*return_vals = values;

	values[0].type = GIMP_PDB_STATUS;
	values[0].data.d_status = status;

	/*  Get the specified drawable  */
	drawable = gimp_drawable_get(param[2].data.d_drawable);
	imageId = param[1].data.d_image;

	if (!gimp_drawable_has_alpha(drawable->drawable_id)) {
		values[0].data.d_status = GIMP_PDB_CALLING_ERROR;
		return;
	}

	gimp_selection_none(imageId);
#ifdef DO_TIMING
	GTimer* timer = g_timer_new();
#endif

	fillZenithAndNadir(drawable);

#ifdef DO_TIMING
	gulong nothing;
	g_print("fillZenithAndNadir() took %g seconds.\n", g_timer_elapsed(timer, &nothing));
	g_timer_destroy(timer);
#endif
	gimp_displays_flush();
	gimp_drawable_detach(drawable);

	return;
}

static void
fillZenithAndNadir(GimpDrawable *drawable) {
	gint x, y, xIndex, channel, channels, alpha;
	gint x1, y1, x2, y2, height;
	GimpPixelRgn rgn_in, rgn_out;
	guchar *inColumn, *outColumn;
	static const guchar NOT_TRANSPARENT_THR = 250;
	static const guchar TRANSPARENT = 0;
	static const guchar OPAQUE = 255;
	static const gint NOT_FOUND = -1;
	gint *topPixels, *bottomPixels;
	guint32 *sumTop, *sumBottom, countTop = 0, countBottom = 0;
	guchar *topAvg, *bottomAvg, pixelChannelVal, *spread;
	guchar inAlpha;
	gint step;


	gimp_drawable_mask_bounds(drawable->drawable_id,
			&x1, &y1,
			&x2, &y2);
	channels = gimp_drawable_bpp(drawable->drawable_id);
	alpha = channels - 1;
	height = y2 - y1;
	step = MAX(10, (x2 - x1) / 50);

	gimp_pixel_rgn_init(&rgn_in,
			drawable,
			x1, y1,
			x2 - x1, y2 - y1,
			FALSE, FALSE);
	gimp_pixel_rgn_init(&rgn_out,
			drawable,
			x1, y1,
			x2 - x1, y2 - y1,
			TRUE, TRUE);
	gimp_tile_cache_ntiles(((y2 - y1) / gimp_tile_height() + 1)*((x2 - x1) / gimp_tile_width() + 1));

	/* Allocate memory for Input and output columns */
	inColumn = g_new(guchar, channels * (y2 - y1));
	outColumn = g_new(guchar, channels * (y2 - y1));

	/* Allocate mremory for top and bottom defined pixel coordinates */
	topPixels = g_new(gint, x2 - x1);
	bottomPixels = g_new(gint, x2 - x1);

	/* Allocate sum tables */
	sumBottom = g_new(guint32, alpha);
	sumTop = g_new(guint32, alpha);
	for (channel = 0; channel < alpha; channel++) {
		sumTop[channel] = sumBottom[channel] = 0;
	}

	/* Allocate avarage tables */
	topAvg = g_new(guchar, alpha);
	bottomAvg = g_new(guchar, alpha);
	spread = g_new(guchar, alpha);

	// First pass, calculating top & bottom pixel positions, and top and bottom averages.

	gimp_progress_init("Calculation of averages and panorama border");
	for (x = x1; x < x2; x++) {
		// Get collumn
		xIndex = x - x1;
		gimp_pixel_rgn_get_col(&rgn_in, inColumn, x, y1, height);
		// Iterate over column from bottom to top to find bottom pixel
		bottomPixels[xIndex] = NOT_FOUND;
		for (y = 0; y < height; y++) {
			guchar alphaValue = inColumn[channels * y + alpha];
			if (alphaValue > NOT_TRANSPARENT_THR) {
				bottomPixels[xIndex] = y;
				for (channel = 0; channel < alpha; channel++) {
					sumBottom[channel] += inColumn[channels * y + channel];
				}
				countBottom++;
				break;
			}
		}
		// Iterate over column from top to bottom to find top pixel
		topPixels[xIndex] = NOT_FOUND;
		for (y = height - 1; y >= 0; y--) {
			guchar alphaValue = inColumn[channels * y + alpha];
			if (alphaValue > NOT_TRANSPARENT_THR) {
				topPixels[xIndex] = y;
				for (channel = 0; channel < alpha; channel++) {
					sumTop[channel] += inColumn[channels * y + channel];
				}
				countTop++;
				break;
			}
		}
		if (x % step == 0)
			gimp_progress_update((gdouble) (x - x1) / (gdouble) (x2 - x1));
	}

	// Calculate averages

	gimp_progress_init("Filling zenith and nadir");
	gimp_progress_update(0.0);
	for (channel = 0; channel < alpha; channel++) {
		topAvg[channel] = sumTop[channel] / countTop;
		bottomAvg[channel] = sumBottom[channel] / countBottom;
		spread[channel] = topAvg[channel] - bottomAvg[channel];
	}

	// Second pass, fill transparent areas. Make everything opaqe.
	for (x = x1; x < x2; x++) {
		xIndex = x - x1;
		gimp_pixel_rgn_get_col(&rgn_in, inColumn, x, y1, height);
		// Fill whole column, when opaque region was not fount
		if (bottomPixels[xIndex] == NOT_FOUND) {
			for (y = 0; y < height; y++) {
				inAlpha = inColumn[channels * y + alpha];
				// Claculate pixel color value
				for (channel = 0; channel < alpha; channel++) {
					pixelChannelVal = spread[channel] * y / height;
					pixelChannelVal += bottomAvg[channel];
					outColumn[channels * y + alpha] = OPAQUE;
					if (inAlpha == TRANSPARENT) {
						outColumn[channels * y + channel] = pixelChannelVal;
					} else {
						outColumn[channels * y + channel] = MAX(255, inAlpha * inColumn[channels * y + channel] / 255 + (OPAQUE - inAlpha) * pixelChannelVal / 255);
					}
				}
			}
		} else {
			// Fill top and bottom regions, as well ad set middle opaque.
			// Bottom
			for (y = 0; y < bottomPixels[xIndex]; y++) {
				// Claculate pixel color value
				inAlpha = inColumn[channels * y + alpha];
				for (channel = 0; channel < alpha; channel++) {
					pixelChannelVal = (inColumn[channels * bottomPixels[xIndex] + channel] - bottomAvg[channel]) * y / bottomPixels[xIndex];
					pixelChannelVal += bottomAvg[channel];
					outColumn[channels * y + alpha] = OPAQUE;
					if (inAlpha == TRANSPARENT) {
						outColumn[channels * y + channel] = pixelChannelVal;
					} else {
						outColumn[channels * y + channel] = MIN(255, inAlpha * inColumn[channels * y + channel] / 255 + (OPAQUE - inAlpha) * pixelChannelVal / 255);
					}
				}
			}
			//Middle - just set to opaque
			for (y = bottomPixels[xIndex]; y <= topPixels[xIndex]; y++) {
				for (channel = 0; channel < alpha; channel++) {
					outColumn[channels * y + channel] = inColumn[channels * y + channel];
				}
				outColumn[channels * y + alpha] = OPAQUE;
			}

			// Top
			for (y = topPixels[xIndex] + 1; y < y2; y++) {
				// Claculate pixel color value
				gint len = y2 - topPixels[xIndex];
				inAlpha = inColumn[channels * y + alpha];
				for (channel = 0; channel < alpha; channel++) {
					pixelChannelVal = (topAvg[channel] - inColumn[channels * topPixels[xIndex] + channel]) * (y - topPixels[xIndex]) / len;
					pixelChannelVal += inColumn[channels * topPixels[xIndex] + channel];
					outColumn[channels * y + alpha] = OPAQUE;
					if (inAlpha == TRANSPARENT) {
						outColumn[channels * y + channel] = pixelChannelVal;
					} else {
						outColumn[channels * y + channel] = MIN(255, inAlpha * inColumn[channels * y + channel] / 255 + (OPAQUE - inAlpha) * pixelChannelVal / 255);
					}
				}
			}

		}

		gimp_pixel_rgn_set_col(&rgn_out, outColumn, x, y1, height);

		if (x % step == 0)
			gimp_progress_update((gdouble) (x - x1) / (gdouble) (x2 - x1));

	}


	g_free(inColumn);
	g_free(outColumn);
	g_free(topPixels);
	g_free(bottomPixels);
	g_free(sumBottom);
	g_free(sumTop);
	g_free(topAvg);
	g_free(bottomAvg);
	g_free(spread);

	gimp_progress_init("Applying fill");

	gimp_drawable_flush(drawable);
	gimp_drawable_merge_shadow(drawable->drawable_id, TRUE);
	gimp_drawable_update(drawable->drawable_id,
			x1, y1,
			x2 - x1, y2 - y1);
}

