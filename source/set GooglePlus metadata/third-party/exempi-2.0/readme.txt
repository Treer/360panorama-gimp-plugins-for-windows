Contains files from a compiled instance of exempi
http://libopenraw.freedesktop.org/wiki/Exempi

I happened to use version 2.2.2 from https://github.com/hfiguiere/exempi/releases/tag/2.2.2

When filled out, your directory structure here should look something like this:
(only shows the directories that matter for building GIMP plugins)

│   readme.txt
│
├───exempi
│       exempi.cpp
│       xmp.h
│       xmpconsts.h
│       xmperrors.h
│
└───public
    │
    ├───include
    │   │   TXMPFiles.hpp
    │   │   TXMPIterator.hpp
    │   │   TXMPMeta.hpp
    │   │   TXMPUtils.hpp
    │   │   XMP.hpp
    │   │   XMP.incl_cpp
    │   │   XMP_Const.h
    │   │   XMP_Environment.h
    │   │   XMP_UnixEndian.h
    │   │   XMP_Version.h
    │   │
    │   └───client-glue
    │           TXMPFiles.incl_cpp
    │           TXMPIterator.incl_cpp
    │           TXMPMeta.incl_cpp
    │           TXMPUtils.incl_cpp
    │           WXMPFiles.hpp
    │           WXMPIterator.hpp
    │           WXMPMeta.hpp
    │           WXMPUtils.hpp
    │           WXMP_Common.hpp
    │
    └───libraries
        ├───windows
            ├───debug
            │       XMPCoreStatic.lib
            │       XMPFilesStatic.lib
            │
            └───release
                    XMPCoreStatic.lib
                    XMPFilesStatic.lib